﻿#include "raylib.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "classPelota.h"


#define π 3.14

Texture2D tablero;
Texture2D pelotas2D[16];

void PosicionarPelotas(pelota* balls[cantidadPelotas]);
void collitionChekPelotas(pelota* balls[cantidadPelotas]);
//----------------------------------------------------------------------------------
// Types and Structures Definition
//----------------------------------------------------------------------------------
pelota* pBlanca;
taco* taquito;
pelota* pelotas[cantidadPelotas];
//typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;
Color colores[cantidadPelotas] = { WHITE,DARKPURPLE/*,GOLD,ORANGE,SKYBLUE,DARKBLUE,VIOLET,BROWN,YELLOW,DARKPURPLE,GOLD,ORANGE,SKYBLUE,DARKBLUE,VIOLET,RED*/ };
Rectangle linea;
Rectangle linea2;
Vector2 PuntoMedio;

//------------------------------------------------------------------------------------
// Global Variables Declaration
//------------------------------------------------------------------------------------
static const int screenWidth = 800;
static const int screenHeight = 450;

static bool gameOver = false;
static bool pause = false;


//------------------------------------------------------------------------------------
// Module Functions Declaration (local)
//------------------------------------------------------------------------------------
static void InitGame(void);         // Initialize game
static void UpdateGame(void);       // Update game (one frame)
static void DrawGame(void);         // Draw game (one frame)
static void UnloadGame(void);       // Unload game
static void UpdateDrawFrame(void);  // Update and Draw (one frame)

//------------------------------------------------------------------------------------
// Program main entry point
//------------------------------------------------------------------------------------
int main(void)
{
    // Initialization (Note windowTitle is unused on Android)
    //---------------------------------------------------------
    InitWindow(screenWidth, screenHeight, "POOL");
    
    tablero = LoadTexture("../res/table.png");
    tablero.height = GetScreenHeight();
    tablero.width = GetScreenWidth();
    
    pelotas2D[0] = LoadTexture("../res/ball 1.png");
    pelotas2D[1] = LoadTexture("../res/ball 2.png");
    pelotas2D[2] = LoadTexture("../res/ball 3.png");
    pelotas2D[3] = LoadTexture("../res/ball 4.png");
    pelotas2D[4] = LoadTexture("../res/ball 5.png");
    pelotas2D[5] = LoadTexture("../res/ball 6.png");
    pelotas2D[6] = LoadTexture("../res/ball 7.png");
    pelotas2D[7] = LoadTexture("../res/ball 8.png");
    pelotas2D[8] = LoadTexture("../res/ball 9.png");
    pelotas2D[9] = LoadTexture("../res/ball 10.png");
    pelotas2D[10] = LoadTexture("../res/ball 11.png");
    pelotas2D[11] = LoadTexture("../res/ball 12.png");
    pelotas2D[12] = LoadTexture("../res/ball 13.png");
    pelotas2D[13] = LoadTexture("../res/ball 14.png");
    pelotas2D[14] = LoadTexture("../res/ball 15.png");
    pelotas2D[15] = LoadTexture("../res/ball 16.png");

    InitGame();

    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update and Draw
        //----------------------------------------------------------------------------------
        UpdateDrawFrame();  
        DrawTexture(tablero, 0, 0, WHITE);        
        for(short i = 0; i < 16; i++)
        {
            pelotas2D[i].height = 30;
            pelotas2D[i].width = 30;
            DrawTexture(pelotas2D[i], 70 + i * pelotas2D[i].width, 100, WHITE);
        }
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadGame();         // Unload loaded data (textures, sounds, models...)

    UnloadTexture(tablero);   

    for(short i = 0; i < cantidadPelotas; i++)
    {
        UnloadTexture(pelotas2D[i]);
    }

    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}

//------------------------------------------------------------------------------------
// Module Functions Definitions (local)
//------------------------------------------------------------------------------------

// Initialize game variables
void InitGame(void)
{
    taquito = new taco();
    for (int i = 0; i < cantidadPelotas; i++)
    {
        pelotas[i] = new pelota({ 0,0 },colores[i],i);
    }
    PosicionarPelotas(pelotas);
    
}

// Update game (one frame)
void UpdateGame(void)
{
    if (!gameOver)
    {
        if (IsKeyPressed('P')) gameOver = !gameOver;
        pelotas[0]->pegarle(taquito->_fuerza);
        pelotas[0]->datos();
        pelotas[0]->drawPDeImpc(GetMousePosition());
        pelotas[0]->move();
        for (int i = 0; i < cantidadPelotas; i++)
        {
            pelotas[i]->actualizar();
            pelotas[i]->golpearPared();
            pelotas[i]->reiniciar();
            //pelotas[i]->stop();
        }
        collitionChekPelotas(pelotas);
    }
}

// Draw game (one frame)
void DrawGame(void)
{
    BeginDrawing();

    ClearBackground(GREEN);
    for (int i = 0; i < cantidadPelotas; i++)
    {
        pelotas[i]->drawPelota(i);
        if (CheckCollisionPointCircle(GetMousePosition(), pelotas[i]->darPos(), pelotas[i]->darRadio()))
            std::cout << "Soy la p " << i << "esta pelota esta en " << pelotas[i]->darPos().x << "," << pelotas[i]->darPos().y;
    }
    DrawLine(linea.x, linea.y, linea.width, linea.height, RED);
    DrawLine(linea2.x, linea2.y, linea2.width, linea2.height, RED);
    EndDrawing();
}

// Unload game variables
void UnloadGame(void)
{
    // TODO: Unload all dynamic loaded data (textures, sounds, models...)
}

// Update and Draw (one frame)
void UpdateDrawFrame(void)
{
    UpdateGame();
    DrawGame();
}
void espejar(pelota* balls[cantidadPelotas])
{
    for (int i = 0; i < cantidadPelotas; i++)
    {
        balls[i]->setPosition({ balls[i]->darPos().x + static_cast<float>(GetScreenWidth() / 2),balls[i]->darPos().y });
    }
}
void PosicionarPelotas(pelota* balls[cantidadPelotas])
{
    float screenHeight = static_cast<float>(GetScreenHeight());
    Vector2 aux = { GetScreenWidth()/10*7, screenHeight / 2 };
    balls[0]->setPosition(aux);
    Rectangle POOL_TABLE = { 0,0,GetScreenWidth(),GetScreenHeight() };
    float POOL_TABLE_FRAME_SIZE = 3;
    float SQRT_THREE = 1.73205f;
    float posicionX = POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + POOL_TABLE.width * 0.125f;
    aux = { posicionX, screenHeight / 2 };
    balls[1]->setPosition(aux);
    balls[0]->setPosition({ posicionX-200, screenHeight / 2 });
    balls[1]->setPosition({ posicionX+2, screenHeight / 2 });
    /*aux = { posicionX+2, (screenHeight / 2) + balls[1]->darRadio() * 2 };
    balls[2]->setPosition(aux);
    aux = { posicionX+2, (screenHeight / 2) + balls[1]->darRadio() * 4 };
    balls[3]->setPosition(aux);
    aux = { posicionX+2, (screenHeight / 2) - balls[1]->darRadio() * 2 };
    balls[4]->setPosition(aux);
    aux = { posicionX+2, (screenHeight / 2) - balls[1]->darRadio() * 4 };
    balls[5]->setPosition(aux);
    aux = { posicionX - SQRT_THREE * balls[1]->darRadio(), (screenHeight / 2) + balls[1]->darRadio() * 3 };
    balls[6]->setPosition(aux);
    aux = { posicionX - SQRT_THREE * balls[1]->darRadio(), (screenHeight / 2) + balls[1]->darRadio() };
    balls[7]->setPosition(aux);
    aux = { posicionX - SQRT_THREE * balls[1]->darRadio(), (screenHeight / 2) - balls[1]->darRadio() * 3 };
    balls[8]->setPosition(aux);
    aux = { posicionX - SQRT_THREE * balls[1]->darRadio(), (screenHeight / 2) - balls[1]->darRadio() };
    balls[9]->setPosition(aux);
    aux = { posicionX - SQRT_THREE * balls[1]->darRadio() * 2, screenHeight / 2 };
    balls[10]->setPosition(aux);
    aux = { posicionX - SQRT_THREE * balls[1]->darRadio() * 2, (screenHeight / 2) + balls[1]->darRadio() * 2 };
    balls[11]->setPosition(aux);
    aux = { posicionX - SQRT_THREE * balls[1]->darRadio() * 2, (screenHeight / 2) - balls[1]->darRadio() * 2 };
    balls[12]->setPosition(aux);
    aux = { posicionX - SQRT_THREE * balls[1]->darRadio() * 3, (screenHeight / 2) + balls[1]->darRadio() };
    balls[13]->setPosition(aux);
    aux = { posicionX - SQRT_THREE * balls[1]->darRadio() * 3, (screenHeight / 2) - balls[1]->darRadio() };
    balls[14]->setPosition(aux);
    aux = { posicionX - SQRT_THREE * balls[1]->darRadio() * 4, screenHeight / 2 };
    balls[15]->setPosition(aux);*/
    espejar(balls);

}
float colisionaron(pelota* balls1, pelota* balls2)
{
    float opusto = fabs(balls1->darPos().y - balls2->darPos().y);
    float adyacente = fabs(balls1->darPos().x - balls2->darPos().x);
    float anguloRadeanes = atan2(opusto, adyacente);
    float angulo = anguloRadeanes * (180 / π);
    float anguloB = 90 - angulo;
    float anguloA = 90 + anguloB;
    Vector2 porcentajeDeVariacion;
    Vector2 aux;
    porcentajeDeVariacion = { 0,0 };
    linea = { balls1->darPos().x,balls1->darPos().y ,balls2->darPos().x,balls2->darPos().y };
    if (angulo>45)
    {
        porcentajeDeVariacion.y = 100;
        porcentajeDeVariacion.x = 100 * angulo / 45;
    }
    else
    {
        porcentajeDeVariacion.y = 100 * angulo / 45;
        porcentajeDeVariacion.x = 100;
    }
   
    if (balls1->darPos().x>= balls2->darPos().x)
    {
        
        balls2->setVel({ balls1->darVel().x * porcentajeDeVariacion.x / 100, balls2->darVel().y });//pero en porcentaje de el angulo.
       // balls1->setVel({ balls1->darVel().x + balls2->darVel().x,balls1->darVel().y});
    }
    else
    {
       
        balls2->setVel({ balls1->darVel().x * fabs(porcentajeDeVariacion.x / 100), balls2->darVel().y });//pero en porcentaje de el angulo.
        //balls1->setVel({ balls1->darVel().x - balls2->darVel().x,balls1->darVel().y});
    }
    if (balls1->darPos().y >= balls2->darPos().y)
    {
       
        balls2->setVel({ balls2->darVel().x,balls1->darVel().y * porcentajeDeVariacion.y/100 });//pero en porcentaje de el angulo.
       // balls1->setVel({ balls1->darVel().x,balls1->darVel().y + balls2->darVel().y });
    }
    else
    {
        
        balls2->setVel({ balls2->darVel().x,balls1->darVel().y * fabs(porcentajeDeVariacion.y / 100) });//pero en porcentaje de el angulo.
       // balls1->setVel({ balls1->darVel().x,balls1->darVel().y - balls2->darVel().y });
    }
    
    linea2 = { PuntoMedio.x-20, PuntoMedio.y-20,PuntoMedio.x+20,PuntoMedio.y+20 };

    return  angulo;
}
void collitionChekPelotas(pelota* balls[cantidadPelotas])
{
    for (int i = 0; i < cantidadPelotas; i++)
    {
        for (int w = i+1; w < cantidadPelotas; w++)
        {
            if (i != w)
            {
                if (CheckCollisionCircles(balls[i]->darPos(), balls[i]->darRadio(), balls[w]->darPos(), balls[w]->darRadio())) //si colicionan.
                {
                   // if (IsKeyPressed(KEY_H))
                    {
                       /* if (i==0)
                        {
                            std::cout << std::endl;
                            std::cout << "angulo: " << colisionaron(balls[i], balls[w]) << std::endl;
                        }*/
                        std::cout << "angulo: " << colisionaron(balls[i], balls[w]) <<"bola "<<i<<" y bola "<<w<< std::endl;
                    }
             
                }
            }
        }
    }
}

